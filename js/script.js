//Setting Up Variables
let theUnput        = document.querySelector('.add-task input');
let theAddButton    = document.querySelector('.add-task .plus');
let taskContainer   = document.querySelector('.tasks-content');

let tasksCount = document.querySelector('.tasks-status .tasks-count span');
let tasksCompleted = document.querySelector('.tasks-status .tasks-completed span');

//Focus On Input Field 
window.onload = function(){
    theUnput.focus();
}

//Adding the Task

theAddButton.onclick = function(){
    //if input is empty
    if(theUnput.value === ''){
        console.log('no value');
    }else{

        let noTasksMsg =  document.querySelector('.no-tasks-messages');

        //Check if span with no tasks msg IS exits
        if(document.body.contains(noTasksMsg)){
            //Remove No Tasks Message
            noTasksMsg.remove();
        }
        //console.log(theUnput.value);
        

        //create span elemet
        let mainSpaqn = document.createElement("span");

        //create delete Button
        let deleteElement = document.createElement("span");

        //Create The Span text
        let text = document.createTextNode(theUnput.value);

        //Create The delete Button Text
         let deleteText = document.createTextNode("Delete");

         //Add Text To Main Span
         mainSpaqn.appendChild(text);

         //Add Class To Main Span 
         mainSpaqn.className = "task-box";

         //Add Text To Delete Button
         deleteElement.appendChild(deleteText);
         
         //Add Class To Delete Button 
         deleteElement.className = "delete";

         //Add Delete Button To Main Span
         mainSpaqn.appendChild(deleteElement);

         //Add The Task To The Container
         taskContainer.appendChild (mainSpaqn);


        // ## Empty  The Input ##
        theUnput.value = '';
        theUnput.focus();

    }
    
}
document.addEventListener("click",function(e){

    //Delete Task
    if(e.target.className == 'delete'){

       // console.log("Delete");
       e.target.parentNode.remove();

       //Check Number of Taskk inside the continer
       if(taskContainer.childElementCount == 0){
        createNoTasksMsg();
       }
    }

    //Finish task
    if(e.target.classList.contains('task-box')){

        // Toogle class 'finished'
        e.target.classList.toggle('finished');
     }
     
    //Calculate Tasks
    calculateTasks()
 
});

//function to create no tasks msg
function createNoTasksMsg(){
    //create message span element
    let msgSpan = document.createElement("span");

    //create the text msg
    let msgText = document.createTextNode("No Tasks  To Show");

    //Add text To Span
    msgSpan.appendChild(msgText);

    //Add Class To Msg Span
    msgSpan.className = "no-tasks-messages";

    //Append the message span element to the task container
    taskContainer.appendChild(msgSpan);
}

//function to calculate tasks
function calculateTasks(){

    //calculate All Tasks
    tasksCount.innerHTML = document.querySelectorAll('.tasks-content .task-box').length;

    //calculate complated Tasks
    tasksCompleted.innerHTML = document.querySelectorAll('.tasks-content .finished').length;

}